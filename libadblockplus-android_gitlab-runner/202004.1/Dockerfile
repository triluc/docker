# Copyright (c) 2020-present eyeo GmbH
#
# This module is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

FROM ubuntu:18.04

# Settings used later on in Dockerfile.
ARG WORK_DIR="/opt/ci"
ARG TIMEZONE="Europe/Berlin"

# Adjust env.
ENV LC_CTYPE="en_US.UTF-8"

# Set timezone and create our workdir
RUN ln -snf /usr/share/zoneinfo/$TIMEZONE /etc/localtime \
&&  echo $TIMEZONE > /etc/timezone \
&&  mkdir $WORK_DIR

# Install generic system packages before libabp-android specific dependencies (below the line break)
RUN echo ttf-mscorefonts-installer msttcorefonts/accepted-mscorefonts-eula select true | debconf-set-selections \
&&  apt-get update \
&&  apt-get install -qy --no-install-recommends \
        build-essential \
        ca-certificates \
        ccache \
        curl \
        dumb-init \
        git \
        lsb-core \
        p7zip-full \
        python \
        sudo \
        unzip \
        wget \
\
\
        npm \
        openjdk-8-jdk \
        clang \
        libc++-dev \
        libc++abi-dev \
&&  rm -rf /var/lib/apt/lists/*

# Install the android NDK.
ENV ANDROID_NDK_ROOT=/opt/android-ndk-r16b
ENV ANDROID_NDK_HOME=/opt/android-ndk-r16b
RUN wget --quiet https://dl.google.com/android/repository/android-ndk-r16b-linux-x86_64.zip -O /tmp/android-ndk.zip \
&&  unzip -q /tmp/android-ndk.zip -d /opt \
&&  rm /tmp/android-ndk.zip

# Install Android SDK tools
ENV ANDROID_HOME=/opt/android-sdk
RUN wget --quiet https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip -O /tmp/sdk-tools.zip \
&&  unzip -q /tmp/sdk-tools.zip -d /opt/android-sdk \
&&  rm /tmp/sdk-tools.zip
RUN echo y | /opt/android-sdk/tools/bin/sdkmanager "build-tools;28.0.3" "platforms;android-28" | grep -v =
