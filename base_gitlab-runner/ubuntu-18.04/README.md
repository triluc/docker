# base_gitlab-runner Docker Image

This image contains a basic 18.04 Ubuntu install along with the [gitlab-runner](https://docs.gitlab.com/runner/)
binary and core configuration needed to get started with a custom CI runner for a project.

It is expected that projects build upon this image using something like `FROM registry.gitlab.com/eyeo/docker/base_gitlab-runner:18.04`
in their `Dockerfile` and install any project-specific package and configuration.


## Running as a development environment

This image (and any built using it as a base) can also be used as a disposable development environment. This can help
maintain a repeatable environment, consistent with CI. Getting a shell into a container is as simple as...

```
docker exec -it registry.gitlab.com/eyeo/docker/base_gitlab-runner:18.04 bash
```

## Running images as a systemd service

Instances of these images are usually quite long-lived, a quick tip is to use something like systemd to control it. An example systemd unit file is included for reference...

```
[Unit]
Description=abpchromium gitlab runner docker container
After=docker.service
Requires=docker.service

[Service]
TimeoutStartSec=0
Restart=always
Environment=DOCKER_IMG='registry.gitlab.com/eyeo/docker/base_gitlab-runner:legacy'
ExecStartPre=-/usr/bin/docker stop %n
ExecStartPre=-/usr/bin/docker rm %n
ExecStartPre=/usr/bin/docker pull ${DOCKER_IMG}
ExecStart=/usr/bin/docker run --rm --name %n \
    -v /opt/eyeo/base_gitlab-runner/etc_gitlab-runner:/etc/gitlab-runner \
    -v some-vol:/opt/ci \
    ${DOCKER_IMG}

[Install]
WantedBy=multi-user.targetcode
```
