# eyeo Docker Resources

This repository is a collection of [Docker][1] resources used to assemble
`registry.gitlab.com/eyeo/docker` containers.

# Adding containers and versions

A container can be added to the collection by copying a version of it into
`<container_name>/<version>/` or adding a git submodule. Please note that the
structure `<container_name>/<version>` is mandatory.

**After adding a container / a version** you need to also add a new CI job for
each of these new items.
Executing `$ ./update_images.py` will examine the project's structure and
create a job for each container / version in `images.yml`. When these
changes are commited. `.gitlab-ci.yml` will include each of those jobs.

You will also need to execute this when a container / version is removed.

# Testing containers

The standard CI workflow includes running Google's
[container structure tests](https://github.com/GoogleContainerTools/container-structure-test)
against images before pushing them to the registry.

To run these tests simply create a file named `cs-tests.yml` alongside your `Dockerfile`
containing the tests you would like to run. In the case of a test failure the job will
fail and the image will not be pushed.

# Folders vs submodules

Images that are added as regular folders are only rebuild when something has
changed in the respective folders. Due to the architecture of GitLab's
[only: changes][2] feature, this is not possible for submodules. Images added
as submodules hence will be rebuild everytime a push happens, regardless of
whether something has changed.

[1]: https://www.docker.com/
[2]: https://docs.gitlab.com/ee/ci/yaml/#onlychangesexceptchanges
